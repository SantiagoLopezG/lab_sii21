#pragma once 
#include "Esfera.h"
#include "Raqueta.h"

enum class action{
	ABAJO = -1,
	NADA,
	ARRIBA
};
class DatosMemCompartida
{       
public:         
      Esfera esfera;
      Raqueta raqueta1;
      action accion;
};
