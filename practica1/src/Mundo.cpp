// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "Mundo.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include<iostream>
#include<string>
#include <sys/mman.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include"Puntuaciones.h"



 
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
//Variable global numero de disparos
//////////////////////////////////////////////////////////////////////

#define NMAXDISPAROS 10
int nDisparosJ1 = 0;
int nDisparosJ2 = 0;
#define NMAXESFERAS 2
#define PENALIZACION -0.05
#define PERIODO 100
#define MAXGOLPES 5
int golpes1=0;
int golpes2=0;

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	close (fd);
	munmap(pDatosMCp,sizeof(datosMemComp));
	unlink("datosMemCompartida");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	for(i=0;i<disparos.size();i++)
		disparos[i]->Dibuja();	
	
	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();
	
	for(i=0;i<esferas.size();i++)
		esferas[i]->Dibuja();	

	

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	static int timer=0;
	static Puntuaciones puntuaciones;
	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);

	
	/********* REBOTE PAREDES - ESFERA/RAQUETAS 	**********/
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}
	
	/********* MOVIMIENTO DISPAROS ***************/
	for(i=0;i<disparos.size();i++){
		disparos[i]->Mueve(0.025f);
	}
	
	/********* MOVIMIENTO ESFERAS *****************/
/*	for(i=0;i<esferas.size();i++){
		esferas[i]->Mueve(0.025f);
	}
*/
	/********* REBOTE ESFERA-RAQUETAS ******************/
	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	
	/********* GOL JUGADOR 2 ******************/
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		puntuaciones.lastWinner = 2;
		puntuaciones.jugador2 = puntos2;
		//esferas.push_back(new Esfera()); 
		/********* ESCRITURA EN PIPE *****************/
		
		printf("GOL J%d\nJ1 %d - %d J2\n", puntuaciones.lastWinner, puntuaciones.jugador1, puntuaciones.jugador2);
		write(fd, &puntuaciones, sizeof(puntuaciones));
	}
	
	/********* GOL JUGADOR 1 ******************/
	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		esfera.radio=0.5f;
		puntos1++;
		puntuaciones.lastWinner = 1;
		puntuaciones.jugador1 = puntos1;
		//esferas.push_back(new Esfera()); 
		/********* ESCRITURA EN PIPE *****************/
		printf("GOL J%d\nJ1 %d - %d J2\n", puntuaciones.lastWinner, puntuaciones.jugador1, puntuaciones.jugador2);
		write(fd, &puntuaciones, sizeof(puntuaciones));
	}
		
	/********* REBOTE ESFERAS-RAQUETAS *****************/
//	for(i=0;i<esferas.size();i++){
//		
//		jugador1.Rebota(*esferas[i]);
//		jugador2.Rebota(*esferas[i]);
//		
//		/********* GOLES JUGADOR 2 *****************/
//		if(fondo_izq.Rebota(*esferas[i]))
//		{
//			esferas[i]->centro.x=0;
//			esferas[i]->centro.y=rand()/(float)RAND_MAX;
//			esferas[i]->velocidad.x=2+2*rand()/(float)RAND_MAX;
//			esferas[i]->velocidad.y=2+2*rand()/(float)RAND_MAX;
//			esferas[i]->radio=0.5f;
//			puntos2++;
//			esferas.push_back(new Esfera()); 
//		}
//		/********* GOLES JUGADOR 1 *****************/
//		if(fondo_dcho.Rebota(*esferas[i]))
//		{
//			esferas[i]->centro.x=0;
//			esferas[i]->centro.y=rand()/(float)RAND_MAX;
//			esferas[i]->velocidad.x=-2-2*rand()/(float)RAND_MAX;
//			esferas[i]->velocidad.y=-2-2*rand()/(float)RAND_MAX;
//			esferas[i]->radio=0.5f;
//			puntos1++;
//			esferas.push_back(new Esfera()); 
//		}
//		/********* REBOTE ESFERAS-PAREDES *****************/
//		for(int j=0;j<paredes.size();j++)
//		{
//			paredes[j].Rebota(*esferas[i]);
//		}
//		
//		/********* TAMAÑO REDUCIENDO ESFERAS *****************/
//		if(esferas[i]->radio >= 0.1f){
//			if(timer % PERIODO == 0)
//				esferas[i]->radio -= 0.05f;
//		}
//		
//	}
	
	/********* LÓGICA DISPAROS - REDUCIR TAMAÑO RAQUETAS *****************/
	for(int j=0;j<disparos.size();j++)
	{
		bool destruirDisparo=false;
			if(jugador1.recibeDisparo(*disparos[j])&&disparos[j]->d==IZQUIERDA){
				if(golpes1<MAXGOLPES){
				jugador1.y1-=PENALIZACION;
				jugador1.y2+=PENALIZACION;
				golpes1++;
				}	
				destruirDisparo=true;
				nDisparosJ2-=1;
			}
			if(jugador2.recibeDisparo(*disparos[j])&&disparos[j]->d==DERECHA){
				if(golpes2<MAXGOLPES){
				jugador2.y1-=PENALIZACION;
				jugador2.y2+=PENALIZACION;
				golpes2++;
				}
				destruirDisparo=true;
				nDisparosJ1-=1;
			}
			if(fondo_izq.Rebota(*disparos[j])||fondo_dcho.Rebota(*disparos[j])){
				if(disparos[j]->d==DERECHA){nDisparosJ1-=1;}
				if(disparos[j]->d==IZQUIERDA){nDisparosJ2-=1;}
				destruirDisparo=true;		
			}
			if(destruirDisparo){
				delete disparos[j];
				disparos.erase(disparos.begin()+j);
				disparos.shrink_to_fit();
			}
			if(timer%PERIODO==0){
				if(golpes1>0){
					jugador1.y1+=PENALIZACION;
					jugador1.y2-=PENALIZACION;
					golpes1--;				
				}
				if(golpes2>0){
					jugador2.y1+=PENALIZACION;
					jugador2.y2-=PENALIZACION;
					golpes2--;				
				}
			
			}
			
		
	}
	
	/**********DATOS MEMORIA COMPARTIDA *****************/
	pDatosMCp->esfera=esfera;
	pDatosMCp->raqueta1=jugador1;	

	if(pDatosMCp->accion==action::ABAJO){OnKeyboardDown('s', 0, 0);}
	else if(pDatosMCp->accion==action::NADA){}
	else if(pDatosMCp->accion==action::ARRIBA){OnKeyboardDown('w', 0, 0);}
	
	//if((puntos1>=3) || (puntos2>=3)) close(fd);
	
	if(esfera.radio >= 0.1f){
		if(timer % PERIODO == 0)
			esfera.radio -= 0.05f;
	}
	
	timer++;	
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;
	case 'd':
		if(nDisparosJ1<NMAXDISPAROS){
			disparos.push_back(new Disparo(DERECHA,jugador1.getCentro()));
			nDisparosJ1+=1;
		}	
		break;
	case 'k':
		if(nDisparosJ2<NMAXDISPAROS){
			disparos.push_back(new Disparo(IZQUIERDA,jugador2.getCentro())); 
			nDisparosJ2+=1;
		}
		break;

	}
}

void CMundo::Init()
{
//INICIALIZACION DE LA FIFO
	if ((fd=open("FIFO", O_WRONLY))<0) {
		perror("[CMundo::Init()] No puede abrirse el FIFO");
		return;
	}
	
//INICIALIZACION MEMORIA COMPARTIDA

	int fd2;
	
	//crea el fichero del tamaño del atributo DatosMemCompartida
	if((fd2 = open("datosMemCompartida", O_CREAT | O_TRUNC | O_RDWR, 0666))<0){
		perror("[CMundo::Init()] Error creacion fichero destino");
		exit(1);
	}
	
	//se introduce la lógica del movimiento de la raqueta 1
	datosMemComp.esfera = esfera;
	datosMemComp.raqueta1 = jugador1;
	datosMemComp.accion = action::NADA;
	
	//se escribe en el fichero compartido
	write(fd2, &datosMemComp, sizeof(datosMemComp));
	
	//se proyecta el fichero en memoeria y
	//se asigna la direccion de comien<o de la region creada al atributo de tipo puntero
	pDatosMCp = static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(datosMemComp), PROT_READ | PROT_WRITE, MAP_SHARED, fd2, 0));
	if(pDatosMCp == MAP_FAILED){
		perror("[CMUndo::Init()] Error en la proyeccion del fichero origen");
		close(fd2);
		return;
	}
	
	//se cierra el descriptor del fichero
	close(fd2);
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
