// Raqueta.cpp: implementation of the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Raqueta.h"
#include <math.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Raqueta::Raqueta()
{
	
}

Raqueta::~Raqueta()
{

}

void Raqueta::Mueve(float t)
{
	y1 = y1 + velocidad.y * t;
	y2 = y2 + velocidad.y * t;
}

bool Raqueta::recibeDisparo(Plano& p){
	Esfera e;
	e.centro.x=(p.x1+p.x2)/2;
	e.centro.y=(p.y1+p.y2)/2;
	e.radio=0.5*sqrt((p.x1-p.x2)*(p.x1-p.x2)+(p.y1-p.y2)*(p.y1-p.y2));

	if(Plano::Rebota(e))
	{
		return true;
	}
	return false;
}
