#include"DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fstream>

int main(){
	DatosMemCompartida *pDatosMemComp;
	
	int fd;
	if((fd=open("datosMemCompartida", O_RDWR | O_CREAT, 0666))<0){
		perror("[bot.cpp -> main()] No se puede abrir el fichero compartido");
		return 1;
	}
	
	printf("[bot.cpp -> main()] Open realizado correctamente\n");
	pDatosMemComp = static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(DatosMemCompartida), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0));
	if(pDatosMemComp == MAP_FAILED){
		perror("[bot.cpp -> main()] No se puede proyectar en memoria el fichero compartido");
		close(fd);
		return 2;
	}
	
	close(fd);

	while(1){
		//bot.cpp y Mundo.cpp comparten puntero de DatosMemCompartida a traves del fichero proyectado en memoria
		if(pDatosMemComp->raqueta1.getCentro().y > pDatosMemComp->esfera.centro.y){
			pDatosMemComp->accion = action::ABAJO;
		}
		else if(pDatosMemComp->raqueta1.getCentro().y < pDatosMemComp->esfera.centro.y){
			pDatosMemComp->accion = action::ARRIBA;
		}
		else 	pDatosMemComp->accion = action::NADA;
		
		usleep(25000);
	}
	
	munmap(pDatosMemComp, sizeof(DatosMemCompartida));
	unlink("datosCompartidos");
	
	return 3;
}
