//logger debe crear la tuberia, abrirla, cerrarla y hacer el unlink
//entra en un bucle infinito de lectura e imprime por salida estandar lo que lee
//Jugador 1 marca 1 punto, lleva un total de X puntos.

//PARA RECORDAR FIFOS MIRAR CLASE 15/10/20 


#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include<string>
#include<iostream>
#include"Puntuaciones.h"



int main(){
	int fd;
	Puntuaciones puntuaciones;
	/* crea el FIFO */
	if (mkfifo("FIFO", 0600)<0) {
		perror("No puede crearse el FIFO");
		return 1;
	}
	
	/*abre el FIFO*/
	if ((fd=open("FIFO", O_RDONLY))<0) {
		perror("[logger.cpp -> main()] No puede abrirse el FIFO");
		return 1;
	}
	
	//printf("[logger.cpp -> main()] open realizado con exito\n");

	/*bucle de lectura del FIFO*/
	//while(puntuaciones.jugador1<3 || puntuaciones.jugador2<3)
	while((read(fd, &puntuaciones, sizeof(puntuaciones)))==sizeof(puntuaciones))
	{
		//read(fd, &puntuaciones, sizeof(puntuaciones));
		if(puntuaciones.lastWinner == 1)
			printf("Jugador 1 marca 1 punto, lleva un total de %d puntos\n", puntuaciones.jugador1);
		else if(puntuaciones.lastWinner == 2)
			printf("Jugador 2 marca 1 punto, lleva un total de %d puntos\n", puntuaciones.jugador2);
	}
	
	/*cierra y deslinkea el FIFO*/
	printf("Jugador %d gana!!!!\n", puntuaciones.lastWinner);
	close(fd);
	unlink("FIFO");
	return 0;
}
